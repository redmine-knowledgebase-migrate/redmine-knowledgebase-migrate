# Redmine Plugin Wiki Article

## 概要

Wikiを利用したお手軽な記事作成をサポートします。

## インストール

```bash
cd redmine
git clone https://gitlab.com/redmine-plugin-wiki-article/redmine-plugin-wiki-article.git plugins/wiki_article
```

## 使い方

### 必要なプラグイン

* Redmine Wiki Extensions Plugin
  * https://www.r-labs.org/projects/r-labs/wiki/Wiki_Extensions_en

### マクロ

#### `{{child_pages_count}}`

子ページの数を表示します。引数を指定しないと、現在のWikiページから子ページの数を表示します。

Examples:

* `{{child_pages_count}}` Wikiページで利用できます
* `{{child_pages_count(depth=2)}}` 深さ2までのページ数のみ表示します
* `{{child_pages_count(Foo)}}` Fooという名前のWikiページからの子ページ数を表示します

#### `{{new_article}}`

新しい記事を作成するためのリンクを追加します。

Examples:

* `{{new_article}}` 新しい記事を作成するフォームをモーダルダイアログとして開きます
* `{{new_article(title=DefaultTitle)}}` 新しい記事を作成するフォームをモーダルダイアログとして開きます。デフォルトのタイトルとして `DefaultTitle` が設定されます
* `{{new_article(template=DefaultTemplate)}}` 新しい記事を作成するフォームをモーダルダイアログとして開きます。デフォルトのテンプレートして `DefaultTemplate` が設定されます
* `{{new_article(modal=false)}}` 新しい記事を作成するフォームに遷移します

### Knowledgebaseプラグインのデータをインポート

[Knowledgebaseプラグイン](https://github.com/alexbevi/redmine_knowledgebase) のデータをWikiへインポートできます。

#### 1. Redmineの停止

環境に合った適切な方法でRedmineを停止します。

#### 2. DBのデータをバックアップ

DBをバックアップします。SQLiteを利用している場合はDBファイルをコピー、
MySQLを利用している場合は以下のようなコマンドでバックアップします。

Example for mysql:

```bash
mysqldump -u USER_NAME -p -h HOST_NAME REDMINE_DB_NAME > redmine_db_backup.sql
```

データ量が多い場合は `gzip` で圧縮することをおすすめします。

```bash
mysqldump -u USER_NAME -p -h HOST_NAME REDMINE_DB_NAME | gzip > redmine_db_backup.sql.gz
```

##### インポート前に戻したい場合

インポートに失敗した場合は、作成したDBのバックアップから戻します。

1. Redmineの停止
2. DBのバックアップデータからインポート
   * SQLiteの場合はバックアップファイルをコピー
   * MySQLの場合のコマンド例
     * 圧縮なし: `cat redmine_db_backup.sql | mysql -u USER_NAME -p -h HOST_NAME REDMINE_DB_NAME`
     * 圧縮あり: `zcat redmine_db_backup.sql.gz | mysql -u USER_NAME -p -h HOST_NAME REDMINE_DB_NAME`
3. Redmineの起動

**Notes: 「4. データインポートのコマンドを実行する」の前に、
作成したバックアップからリストアできることを別のDBで確認するとより安全です。**

#### 3. データインポートの準備

任意のディレクトリに3つのファイルを作成する必要があります。
ファイル名は固定です。
拡張子はテキストフォーマットに合わせて設定してください。

* knowledgebase-top.{textile, md}
  * Knowledgebaseのトップとなるページの内容を記載
* category-top.{textile, md}
  * KnowledgebaseのカテゴリはWikiページとしてインポートします
  * その内容を記載します
  * カテゴリ名を挿入するための `%{name}` は必ず含めてください
* article-template.{textile, md}
  * Knowledgebaseの記事はWikiページとしてインポートします
  * 記事のタイトルは最上部に自動で挿入されます
  * 概要を挿入するための `%{summary}` は必ず含めてください
  * 説明を挿入するための `%{description}` は必ず含めてください

それぞれの例は `examples/` ディレクトリにあります。

#### 4. データインポートのコマンドを実行する

適宜、環境変数を設定します。

* `CONTENT_DIR`
  * 準備で作成した3つのファイルが置いてあるディレクトリ
  * デフォルトは `examples/${REDMINE_LANG}/` です
* `TEXT_FORMATTING`
  * 利用しているテキストフォーマット。 `textile` or `md`
  * デフォルトは `textile` です
* `TOP_PAGE_TITLE`
  * KnowledgebaseのトップとなるWikiページのページ名
  * デフォルトは `Knowledgebase` です
* `ARTICLE_TEMPLATE_NAME`
  * 記事作成用のテンプレートを設定するWikiのページ名
  * デフォルトは `ArticleTemplate` です
* `REDMINE_LANG`
  * 本プラグインでは `CONTENT_DIR` のデフォルト値で利用します
  * デフォルトは `en` です

環境変数の設定が終わったらコマンドを実行します。

Knowledgebaseプラグインが有効になっているプロジェクトについて、Wikiへのデータインポートを行います。

`examples/ja/*` のファイルを利用してインポートする例:

```bash
REDMINE_LANG=ja RAILS_ENV=production bin/rails wiki_article:knowledgebase:import
```

`examples/ja/*` のファイルを利用、Knowledgebaseのトップページ名を指定してインポートする例:

```bash
REDMINE_LANG=ja TOP_PAGE_TITLE="ナレッジベーストップ" \
  RAILS_ENV=production bin/rails wiki_article:knowledgebase:import
```

**注）データインポートに伴い、ウォッチャー宛に通知が飛ぶ可能性があります。**

#### トラブルシューティング

以下のエラーが出た場合は対応をお願いします。

##### a. `The target projects must enable "wiki" and "wiki_extensions"`

```
The target projects must enable "wiki" and "wiki_extensions": PROJECT_NAME (PROJECT_ID)"
```

対象のプロジェクト（= Knowledgebaseが有効になっているプロジェクト）で `wiki` と `wiki_extensions` が有効になっていません。
それぞれにデータをインポートするので、両方を有効にしてください。

##### b. `Some projects have not yet created a Wiki start page`

```
Some projects have not yet created a Wiki start page: PROJECT_NAME (PROJECT_ID)
```

対象のプロジェクト（= Knowledgebaseが有効になっているプロジェクト）でWikiのスタートページが未作成です。
スタートページ以下にデータをインポートするので、スタートページを作成してください。

##### c. `Could not find template files`

```
Could not find template files: FILE_PATH
```

インポートで利用するテンプレートファイルが見つけられませんでした。
`CONTENT_DIR` に3つのファイルがあるか、 `TEXT_FORMATTING` で指定した拡張子になっているかを確認してください。

* knowledgebase-top.{textile, md}
* category-top.{textile, md}
* article-template.{textile, md}

##### d. `There are articles with the same title in the same project`

```
There are articles with the same title in the same project: "project=PROJECT_NAME(PROJECT_ID) title=ARTICLE_TITLE"
```

Knowledgebaseの記事に同じタイトルの記事があります。
同じプロジェクトで重複している場合にこのエラーが出ます。
Wikiページでは同じ名前のページを作成できないため、Knowledgebaseの記事名を変更してください。

#### 5. Redmineスタート

環境に合った適切な方法でRedmineをスタートします。

#### 6. 確認

Wikiへデータがインポートされるので確認をお願いします。

#### 7. Knowledgebaseのアンインストール

Knowledgebaseプラグインが不要になったらアンインストールします。

参考: https://github.com/alexbevi/redmine_knowledgebase?tab=readme-ov-file#uninstall

ドキュメントのアンインストールコマンドを実行するときの注意点が2つあります。

* 注1）Knowledgebase独自のテーブルはすべて削除されます
* 注2）Redmineの機能を利用している、添付ファイル、コメント、ウォッチャーについてはデータが残ります

##### 補足

アンインストール時に `20150326093122 AddTaggingsCounterCacheToTags: reverting` でエラーが出た場合は
マイグレーションファイルを修正すると良いです。
存在しないカラムを消そうとしているのでコメントアウトで問題ないです。

```diff
diff --git a/db/migrate/20150326093122_add_taggings_counter_cache_to_tags.rb b/db/migrate/20150326093122_add_taggings_counter_cache_to_tags.rb
index 2c4949a..2767022 100644
--- a/db/migrate/20150326093122_add_taggings_counter_cache_to_tags.rb
+++ b/db/migrate/20150326093122_add_taggings_counter_cache_to_tags.rb
@@ -9,6 +9,6 @@ class AddTaggingsCounterCacheToTags < Rails.version < '5.1' ? ActiveRecord::Migr
   end
 
   def self.down
-    remove_column :tags, :taggings_count
+    # remove_column :tags, :taggings_count
   end
 end
```

オープン中の[このプルリクエスト](https://github.com/alexbevi/redmine_knowledgebase/pull/408) でも
[同様の修正](https://github.com/alexbevi/redmine_knowledgebase/pull/408/files#diff-e33b9f323eabde3da05da5e2818e18db9b1cbeb5de86546721843ec03144859eL11-L13) がされています。

## License

GPLv2 or later. See [LICENSE](LICENSE) for details.

#!/usr/bin/env bash

# Copyright (C) 2024  Abe Tomoaki <abe@clear-code.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

set -eux

branch=${1:-4.2}

tar \
  -cf wiki_article.tar \
  -v \
  --exclude-vcs \
  --exclude-vcs-ignores \
  .
git clone \
  --depth 1 \
  --branch ${branch}-stable \
  https://github.com/redmine/redmine.git \
  redmine
mkdir -p redmine/plugins/wiki_article
tar \
  -xf wiki_article.tar \
  -C redmine/plugins/wiki_article
cat <<DATABASE_YML > redmine/config/database.yml
test:
  adapter: sqlite3
  database: db/redmine.test.sqlite3
DATABASE_YML

git clone \
  --depth 1 \
  https://github.com/alexbevi/redmine_knowledgebase.git \
  redmine/plugins/redmine_knowledgebase

git clone \
  --depth 1 \
  https://github.com/haru/redmine_wiki_extensions.git \
  redmine/plugins/redmine_wiki_extensions

cd redmine
bundle install
bin/rails db:create
bin/rails generate_secret_token
bin/rails db:migrate
bin/rails redmine:plugins:migrate
REDMINE_LANG=en bin/rails redmine:load_default_data
cd -

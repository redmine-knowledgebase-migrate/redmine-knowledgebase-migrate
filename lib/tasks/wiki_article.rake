# Copyright (C) 2024  Abe Tomoaki <abe@clear-code.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

namespace :wiki_article do
  desc "Tag"
  task :tag => :environment do
    plugin = Redmine::Plugin.find(:wiki_article)
    version = plugin.version
    cd(plugin.directory) do
      sh("git", "tag",
         "-a", "v#{version}",
         "-m", "#{version} has been released!!!")
      sh("git", "push", "--tags")
    end
  end

  namespace :knowledgebase do
    desc "Verify if it can be imported"
    task :validate => :environment do
      importer = WikiArticle::KnowledgebaseImporter.new

      importer.validate_enabled_modules
      puts 'OK: validate_enabled_modules was valid.'
      importer.validate_wiki_start_page
      puts 'OK: validate_wiki_start_page was valid.'
      importer.validate_duplicated_article_title
      puts 'OK: validate_duplicated_article_title was valid.'
      importer.validate_content_files
      puts 'OK: validate_content_files was valid.'
    end


    wait_queue = lambda do
      queue_adapter = ActiveJob::Base.queue_adapter
      case queue_adapter
      when ActiveJob::QueueAdapters::AsyncAdapter
        queue_adapter.shutdown
      end
    end

    desc "Import Knowledgebase data"
    task :import => :validate do
      ActiveRecord::Base.transaction do
        importer = WikiArticle::KnowledgebaseImporter.new
        importer.import
      end

      wait_queue.call
    end
  end
end

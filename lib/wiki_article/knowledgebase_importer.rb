# Copyright (C) 2024  Abe Tomoaki <abe@clear-code.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

module WikiArticle
  class KnowledgebaseImporter
    class ValidationError < StandardError
    end

    DEFAULT_KNOWLEDGEBASE_TOP_TITLE = 'Knowledgebase'
    DEFAULT_ARTICLE_TEMPLATE_TITLE = 'ArticleTemplate'

    def validate_enabled_modules
      return true if alert_projects.empty?

      alert_project_infos = alert_projects.collect do |project|
        "#{project.name} (#{project.id})"
      end
      raise(
        ValidationError,
        "The target projects must enable \"wiki\" and \"wiki_extensions\": #{alert_project_infos.join(", ")}"
      )
    end

    def validate_wiki_start_page
      no_start_page_projects = target_projects.select do |project|
        project.wiki.nil? || project.wiki.find_page(project.wiki.start_page).nil?
      end
      return true if no_start_page_projects.empty?

      no_start_page_infos = no_start_page_projects.collect do |project|
        "#{project.name} (#{project.id})"
      end
      raise(
        ValidationError,
        "Some projects have not yet created a Wiki start page: #{no_start_page_infos.join(", ")}"
      )
    end

    def validate_content_files
      files = [
        knowledgebase_top_content_file,
        category_top_template_file,
        article_template_file
      ]
      not_exists = files.select do |path|
        !File.exist?(path)
      end
      return true if not_exists.empty?
      raise(
        ValidationError,
        "Could not find template files: #{not_exists.join(", ")}"
      )
    end

    def validate_duplicated_article_title
      duplicated_titles = KbArticle.
        where(project_id: target_projects.collect(&:id)).
        having('count(*) >= ?', 2).
        group(:project_id, :title).
        count
      return true if duplicated_titles.empty?

      error_info = duplicated_titles.keys.collect do |project_id, title|
        project = Project.find(project_id)
        "project=#{project.name}(#{project.id}) title=#{title}"
      end
      raise(
        ValidationError,
        "There are articles with the same title in the same project: #{error_info.join(", ")}"
      )
    end

    def import
      author = User.anonymous

      knowledgebase_top_content = File.read(knowledgebase_top_content_file)
      category_top_template = File.read(category_top_template_file)
      article_template = File.read(article_template_file)

      target_projects.each do |project|
        kb_top = ensure_knowledgebase_top(
          author,
          project,
          knowledgebase_top_content,
          title: ENV['TOP_PAGE_TITLE']
        )
        import_categories(
          author,
          project.wiki,
          category_top_template,
          project.categories,
          kb_top
        )
        import_articles(project, article_template)

        # Category watchers are imported last to avoid unnecessary notifications.
        import_category_watchers(project)

        create_template_page(
          author,
          project.wiki,
          ENV['ARTICLE_TEMPLATE_NAME'] || DEFAULT_ARTICLE_TEMPLATE_TITLE,
          article_template
        )
      end
    end

    private

    def content_dir
      lang = ENV['REDMINE_LANG'] || 'en'
      @content_dir ||= File.expand_path(ENV['CONTENT_DIR'] || File.join(__dir__, '..', '..', 'examples', lang))
    end

    def suffix
      @suffix = ENV['TEXT_FORMATTING'] || 'textile'
    end

    def knowledgebase_top_content_file
      @knowledgebase_top_content_file ||= File.join(content_dir, "knowledgebase-top.#{suffix}")
    end

    def category_top_template_file
      @category_top_template_file ||= File.join(content_dir, "category-top.#{suffix}")
    end

    def article_template_file
      @article_template_file ||= File.join(content_dir, "article-template.#{suffix}")
    end

    def target_projects
      @target_projects ||= Project.has_module(:knowledgebase).has_module(:wiki).has_module(:wiki_extensions)
    end

    def alert_projects
      # Import data into `wiki` and `wiki extensions`.
      # So they must be enabled.
      @alert_projects ||= Project.has_module(:knowledgebase) - target_projects
    end

    def create_wiki_page(author, wiki, title, text, parent: nil)
      page = WikiPage.new(wiki: wiki, title: title)
      page.parent = parent if parent.present?
      content = WikiContent.new(page: page)
      content.text = text
      content.author_id = author.id
      raise ActiveRecord::RecordInvalid.new(page) unless page.save_with_content(content)
      page
    end

    def create_template_page(author, wiki, title, text)
      return if wiki.find_page(title)
      create_wiki_page(author, wiki, title, text)
    end

    def ensure_knowledgebase_top(author, project, content_text, title: nil)
      title ||= DEFAULT_KNOWLEDGEBASE_TOP_TITLE

      kb_top = project.wiki.find_page(title)
      return kb_top if kb_top.present?

      create_wiki_page(
        author,
        project.wiki,
        title,
        content_text,
        parent: project.wiki.find_page(project.wiki.start_page)
      )
    end

    def category_page_name(category)
      'Category-' + category.title
    end

    def create_category_page(author, wiki, template, category, parent)
      title = category_page_name(category)
      text = template % {name: category.title}
      create_wiki_page(author, wiki, title, text, parent: parent)
    end

    def import_categories(author, wiki, template, categories, parent)
      categories.each do |category|
        next if wiki.find_page(category_page_name(category))
        page = create_category_page(author, wiki, template, category, parent)
      end

      categories.where.not(parent_id: nil).each do |category|
        parent = wiki.find_page(category_page_name(KbCategory.find(category.parent_id)))
        page = wiki.find_page(category_page_name(category))
        page.parent = parent
        page.save!
      end
    end

    def import_category_watchers(project)
      # Run after creating the category tops.
      project.categories.each do |category|
        page = project.wiki.find_page(category_page_name(category))
        next unless page
        import_watchers(category, page)
      end
    end

    def article_text(article, template)
      "#{suffix == 'md' ? '#' : 'h1.'} #{article.title}\n" +
        template % {
          summary: article.summary,
          description: article.content
        }
    end

    def last_updater(article)
      if article.version == 1
        return article.author || User.anonymous
      end
      article.updater || User.anonymous
    end

    def import_article_versions(article, page, template)
      article.versions.each do |v|
        next if page.content.versions.where(version: v.version).exists?
        updater = last_updater(v)

        version = WikiContentVersion.new
        version.wiki_content_id = page.content.id
        version.page_id = page.id
        version.author_id = updater.id
        version.data = article_text(v, template)
        version.comments = v.version_comments
        version.updated_on = v.updated_at
        version.version = v.version
        version.save!(touch: false)
      end
    end

    def import_tags(article, page)
      tags = article.tags.all.map { |tag| [tag.id, tag.name] }.to_h
      page.set_tags(tags)
    end

    def import_attachments(article, page)
      article.attachments.each do |kb_attachment|
        next if page.attachments.where(disk_filename: kb_attachment.disk_filename).exists?
        attachment = kb_attachment.dup
        attachment.container_id = page.id
        attachment.container_type = WikiPage
        attachment.created_on = kb_attachment.created_on
        attachment.save!(touch: false)
      end
    end

    def import_comments(article, page)
      article.comments.each do |comment|
        condition = {
          wiki_page_id: page.id,
          user_id: comment.author_id,
          comment: comment.content,
          created_at: comment.created_on,
          updated_at: comment.updated_on
        }
        next if WikiExtensionsComment.where(condition).exists?
        wiki_extensions_comment = WikiExtensionsComment.new
        wiki_extensions_comment.wiki_page_id = page.id
        wiki_extensions_comment.user_id = comment.author_id
        wiki_extensions_comment.comment = comment.content
        wiki_extensions_comment.created_at = comment.created_on
        wiki_extensions_comment.updated_at = comment.updated_on
        wiki_extensions_comment.save!(touch: false)
      end
    end

    def import_view_count(article, page)
      article.viewings.group('date(created_at)').count.each do |date, count|
        next if WikiExtensionsCount.where(page_id: page.id, date: date).exists?
        pv = WikiExtensionsCount.new
        pv.project_id = page.project.id
        pv.page_id = page.id
        pv.date = date
        pv.count = count
        pv.save!(touch: false)
      end
    end

    def validate_watcher_user(user)
      return false if user.nil?
      return true if user.is_a?(Group) && user.givable?
      user.is_a?(User) && user.active?
    end

    def import_watchers(kb_record, page)
      kb_record.watchers.each do |kb_watcher|
        next if page.watchers.where(user_id: kb_watcher.user_id).exists?
        watcher = kb_watcher.dup
        next unless validate_watcher_user(watcher.user)
        watcher.watchable_id = page.id
        watcher.watchable_type = WikiPage
        watcher.save!(touch: false)
      end
    end

    def import_article(wiki, article, template)
      page = WikiPage.new(wiki: wiki, title: article.title)
      page.parent = wiki.find_page(category_page_name(article.category))
      page.created_on = article.created_at

      updater = last_updater(article)
      content = WikiContent.new(page: page)
      content.text = article_text(article, template)
      content.updated_on = article.updated_at
      content.author_id = updater.id
      content.comments = article.version_comments
      content.version = article.version
      raise ActiveRecord::RecordInvalid.new(page) unless page.save_with_content(content)
      page
    end

    def import_articles(project, template)
      KbArticle.where(project_id: project.id).each do |article|
        page = project.wiki.find_page(article.title)
        unless page
          page = import_article(project.wiki, article, template)
        end

        # Notes
        # Saved WikiContent are automatically created WikiContentVersion as well.
        import_article_versions(article, page, template)

        import_tags(article, page)
        import_attachments(article, page)
        import_comments(article, page)
        import_view_count(article, page)
        import_watchers(article, page)
      end
    end
  end
end

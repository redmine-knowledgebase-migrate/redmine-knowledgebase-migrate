# Copyright (C) 2024  Abe Tomoaki <abe@clear-code.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

Redmine::Plugin.register :wiki_article do
  name 'Wiki Article'
  author 'ClearCode Inc.'
  description 'Use Wiki as article platform'
  version '1.1.2'
  url 'https://gitlab.com/redmine-plugin-wiki-article/redmine-plugin-wiki-article'
  author_url 'https://gitlab.com/clear-code'
end

WikiArticle::Macros
WikiController.include(WikiArticle::Templatable)

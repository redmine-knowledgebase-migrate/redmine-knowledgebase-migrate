# Copyright (C) 2024  Sutou Kouhei <kou@clear-code.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

class WikiArticlesController < ApplicationController
  before_action :find_wiki, :wiki_authorize

  def new
    parent = @wiki.find_page(params[:parent])
    @page = WikiPage.new(parent_id: parent&.id,
                         title: params[:title],
                         wiki: @wiki)
    @template = params[:template]
    unless User.current.allowed_to?(:edit_wiki_pages, @project)
      render_403
      return
    end
  end

  private
  def wiki_authorize
    authorize("wiki")
  end

  def find_wiki
    @project = Project.find(params[:project_id])
    @wiki = @project.wiki
    render_404 unless @wiki
  rescue ActiveRecord::RecordNotFound
    render_404
  end
end

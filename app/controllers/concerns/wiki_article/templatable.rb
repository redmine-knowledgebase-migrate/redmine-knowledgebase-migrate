# Copyright (C) 2024  Kodama Takuya <otegami@clear-code.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

module WikiArticle
  module Templatable
    extend ActiveSupport::Concern

    included do
      before_action :set_content_based_on_template, only: :update
    end

    private
    def set_content_based_on_template
      return if params[:id].blank?
      return if params[:template].blank?
      return if params[:content].present?

      template_page = @wiki.find_page(params[:template])
      return if template_page.nil?

      page = @wiki.find_or_new_page(params[:id])
      return render_403 unless editable?(page)
      return unless page.new_record?

      # initialize_page_content may use @page not the given page
      # internally. :<
      @page = page
      params[:content] = {text: <<-TEXT}
#{initial_page_content(page)}

#{template_page.content.text}
      TEXT
      @page = nil
    end
  end
end

# Redmine Plugin Wiki Article

## Description

Use Wiki as article platform.

## Installation

```bash
cd redmine
git clone https://gitlab.com/redmine-plugin-wiki-article/redmine-plugin-wiki-article.git plugins/wiki_article
```

## Usage

### Required plugins

* Redmine Wiki Extensions Plugin
  * https://www.r-labs.org/projects/r-labs/wiki/Wiki_Extensions_en

### Macros

#### `{{child_pages_count}}`

Displays the number of child pages. With no argument, it displays the number of child pages from current wiki page.

Examples:

* `{{child_pages_count}}` can be used from a wiki page only
* `{{child_pages_count(depth=2)}}` display the number of 2 levels nesting pages only
* `{{child_pages_count(Foo)}}` display the number of all children pages from Foo

#### `{{new_article}}`

Add a link to create a new article.

Examples:

* `{{new_article}}` open a form to create a new article as modal dialog
* `{{new_article(title=DefaultTitle)}}` open a form to create a new article as modal dialog with DefaultTitle as the default title
* `{{new_article(template=DefaultTemplate)}}` open a form to create a new article as modal dialog with DefaultTempalte as the default template
* `{{new_article(modal=false)}}` move to a form to create a new article

### Import data from Knowledgebase

Import [Knowledgebase Plugin](https://github.com/alexbevi/redmine_knowledgebase) data into the Wiki.

#### 1. Stop Redmine

Stop Redmine in a way that suits your environment.

#### 2. Backup DB data

If you are using SQLite, copy the DB files.
If you are using MySQL, use the following commands to back up the DB data.

Example for MySQL:

```bash
mysqldump -u USER_NAME -p -h HOST_NAME REDMINE_DB_NAME > redmine_db_backup.sql
```

If you have a large data volume, it is recommended to compress it using `gzip`.

```bash
mysqldump -u USER_NAME -p -h HOST_NAME REDMINE_DB_NAME | gzip > redmine_db_backup.sql.gz
```

##### Revert to before import

If import fails, restore from DB backup.

1. Stop Redmine
2. Import from DB backup data
   * For SQLite, copy backup file
   * Example commands for MySQL
     * Without compression: `cat redmine_db_backup.sql | mysql -u USER_NAME -p -h HOST_NAME REDMINE_DB_NAME`
     * With compression: `zcat redmine_db_backup.sql.gz | mysql -u USER_NAME -p -h HOST_NAME REDMINE_DB_NAME`
3. Start Redmine

**Notes: It is safer to verify with other DB that you can restore from a backup before `4. Run data import command`.**

#### 3. Prepare for data import

You must create three files in a directory.
The file names are fixed.
Extensions should be set according to the text format.

* knowledgebase-top.{textile, md}
  * Knowledgebase top page contents
* category-top.{textile, md}
  * Import Knowledgebase categories as wiki pages
    * Its contents
  * `%{name}` must be included
    * Insert category name
* article-template.{textile, md}
  * Knowledgebase articles are imported as wiki pages
  * Article title automatically is inserted at the top
  * `%{summary}` must be included
    * Insert summary
  * `%{description}` must be included
    * Insert description

Examples of each files can be found in the `examples/` directory.

#### 4. Run data import command

You can specify options with environment variables.

* `CONTENT_DIR`
  * Directory with three files
  * The default is `examples/${REDMINE_LANG}/`
* `TEXT_FORMATTING`
  * Text Format. `textile` or `md`
  * The default is `textile`
* `TOP_PAGE_TITLE`
  * Knowledgebase top wiki page name
  * The default is `Knowledgebase`
* `ARTICLE_TEMPLATE_NAME`
  * Template Wiki page name for article
  * The default is `ArticleTemplate`
* `REDMINE_LANG`
  * Used by default for `CONTENT_DIR`
  * The default is `en`

After setting the environment variables, run the command.

Import data into Wiki for projects that have the Knowledgebase plugin enabled.

Example of import using `examples/en/*` files:

```bash
RAILS_ENV=production bin/rails wiki_article:knowledgebase:import
```

Example of import using files in `examples/en/*` and specifying the Knowledgebase top page name:

```bash
TOP_PAGE_TITLE="KB TOP PAGE!" RAILS_ENV=production bin/rails wiki_article:knowledgebase:import
```

**Note: Notifications may be sent to watchers as data is imported.**

#### Troubleshooting

Please take action in case of the following errors.

##### a. `The target projects must enable "wiki" and "wiki_extensions"`

```
The target projects must enable "wiki" and "wiki_extensions": PROJECT_NAME (PROJECT_ID)"
```

The `wiki` and `wiki_extensions` are not enabled in the target project (= project with Knowledgebase enabled).
Import data into both, so please enable both.

##### b. `Some projects have not yet created a Wiki start page`

```
Some projects have not yet created a Wiki start page: PROJECT_NAME (PROJECT_ID)
```

The Wiki start page has not yet been created in the target project (= project with Knowledgebase enabled).
Please create a start page as data will be imported under the start page.

##### c. `Could not find template files`

```
Could not find template files: FILE_PATH
```

Could not find the template file to use for import.
Please check if there are three files in `CONTENT_DIR` and that they have the extension specified in `TEXT_FORMATTING`.

* knowledgebase-top.{textile, md}
* category-top.{textile, md}
* article-template.{textile, md}

##### d. `There are articles with the same title in the same project`

```
There are articles with the same title in the same project: "project=PROJECT_NAME(PROJECT_ID) title=ARTICLE_TITLE"
```

There were articles with the same title in the Knowledgebase article.
This error occurs when there are duplicates in the same project.
Please change the name of the article in Knowledgebase because it is not allowed to create a wiki page with the same name.

#### 5. Start Redmine

Start Redmine in a way that suits your environment.

#### 6. Check Wiki

Please check the data as it is imported into Wiki.

#### 7. Uninstall Knowledgebase

Uninstall the Knowledgebase plugin when it is no longer needed.

FYI: https://github.com/alexbevi/redmine_knowledgebase?tab=readme-ov-file#uninstall

There are two points to note when running the uninstall command.

* All Knowledgebase tables will be dropped
* Data will remain for attachments, comments, and watchers that use Redmine features

##### Supplement

If you get an error with `20150326093122 AddTaggingsCounterCacheToTags: reverting` when uninstalling,
please modify your migration file.
Comment out because you are trying to drop a column that does not exist.

```diff
diff --git a/db/migrate/20150326093122_add_taggings_counter_cache_to_tags.rb b/db/migrate/20150326093122_add_taggings_counter_cache_to_tags.rb
index 2c4949a..2767022 100644
--- a/db/migrate/20150326093122_add_taggings_counter_cache_to_tags.rb
+++ b/db/migrate/20150326093122_add_taggings_counter_cache_to_tags.rb
@@ -9,6 +9,6 @@ class AddTaggingsCounterCacheToTags < Rails.version < '5.1' ? ActiveRecord::Migr
   end
 
   def self.down
-    remove_column :tags, :taggings_count
+    # remove_column :tags, :taggings_count
   end
 end
```

[The same fix](https://github.com/alexbevi/redmine_knowledgebase/pull/408/files#diff-e33b9f323eabde3da05da5e2818e18db9b1cbeb5de86546721843ec03144859eL11-L13) is included in [this pull request](https://github.com/alexbevi/redmine_knowledgebase/pull/408) that is open.

## For developers

### Release a new version

1. (optional) If there are many additional features, the version number will be updated in `init.rb`
    * Not required for patch release
2. Add an entry for new release to `CHANGELOG.md`
3. Run `bin/rails wiki_article:tag`
    * Run the task in directory of Redmine itself, not in directory of this plugin
    * This task adds a Git tag and push
4. Increment the patch version in `init.rb`

## License

GPLv2 or later. See [LICENSE](LICENSE) for details.

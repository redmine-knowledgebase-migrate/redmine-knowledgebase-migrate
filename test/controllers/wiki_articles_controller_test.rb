# Copyright (C) 2024  Sutou Kouhei <kou@clear-code.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

require File.expand_path('../../test_helper', __FILE__)

class WikiArticlesControllerTest < Redmine::ControllerTest
  tests WikiArticlesController

  fixtures :projects, :users, :roles, :enabled_modules, :wikis,
           :wiki_pages, :wiki_contents, :wiki_content_versions,
           :members, :member_roles

  include Redmine::I18n

  setup do
    @request.session[:user_id] = 2
    @project = Project.find(1)
    @wiki = @project.wiki
    @top_page = @wiki.find_page(@wiki.start_page)
    @parent_page = @wiki.pages.first

    @template_page = @wiki.pages.new(title: "ArticleTemplate")
    @template_page.content =
      WikiContent.new(text: "h2. Description\n\n{description}\n")
    @template_page.save!
  end

  test "parameters: no" do
    get :new, params: {
      project_id: @project.id,
    }
    assert_select("h2", text: I18n.t(:label_wiki_article_article_new))
    assert_select("form.new_page") do
      assert_select("input[name='wiki_page[parent_id]']") do |parent_id,|
        assert_equal(@top_page.id.to_s, parent_id.attr("value"))
      end
      assert_select("input#page_title") do |title,|
        assert_nil(title.attr("value"))
      end
      assert_select("input[name=template]") do |template,|
        assert_nil(template.attr("value"))
      end
    end
  end

  test "parameters: parent" do
    get :new, params: {
      parent: @parent_page.title,
      project_id: @project.id,
    }
    assert_select("h2", text: I18n.t(:label_wiki_article_article_new))
    assert_select("form.new_page") do
      assert_select("input[name='wiki_page[parent_id]']") do |parent_id,|
        assert_equal(@parent_page.id.to_s, parent_id.attr("value"))
      end
      assert_select("input#page_title") do |title,|
        assert_nil(title.attr("value"))
      end
      assert_select("input[name=template]") do |template,|
        assert_nil(template.attr("value"))
      end
    end
  end

  test "parameters: template" do
    get :new, params: {
      project_id: @project.id,
      template: @template_page.title,
    }
    assert_select("h2", text: I18n.t(:label_wiki_article_article_new))
    assert_select("form.new_page") do
      assert_select("input[name='wiki_page[parent_id]']") do |parent_id,|
        assert_equal(@top_page.id.to_s, parent_id.attr("value"))
      end
      assert_select("input#page_title") do |title,|
        assert_nil(title.attr("value"))
      end
      assert_select("input[name=template]") do |template,|
        assert_equal(@template_page.title, template.attr("value"))
      end
    end
  end
end

# Copyright (C) 2024  Kodama Takuya <otegami@clear-code.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

require File.expand_path('../../../test_helper', __FILE__)

module WikiArticle
  class TemplatableTest < Redmine::ControllerTest
    tests WikiController

    fixtures :projects, :users, :roles, :enabled_modules, :wikis,
             :wiki_pages, :wiki_contents, :wiki_content_versions,
             :members, :member_roles

    setup do
      @request.session[:user_id] = 2
      @project = Project.find(1)
      @wiki_page = @project.wiki.pages.first

      @template_page = @project.wiki.pages.new(title: "ArticleTemplate")
      @template_page.content =
        WikiContent.new(text: "h2. Description\n\n{description}\n")
      @template_page.save!
    end

    test "no template" do
      put :update, params: {
        project_id: @project.id,
        id: "Article",
        content: {
          comments: "create",
          text: "h1. Article\n\nCreated!\n",
        },
        wiki_page: {
          parent_id: @wiki_page.id,
        },
      }
      assert_redirected_to "/projects/#{@project.identifier}/wiki/Article"

      page = @project.wiki.pages.find_by_title('Article')
      assert_equal("h1. Article\n\nCreated!\n", page.content.text)
    end

    test "template: no content" do
      put :update, params: {
        project_id: @project.id,
        id: "Article",
        wiki_page: {
          parent_id: @wiki_page.id,
        },
        template: @template_page.title,
      }
      assert_redirected_to "/projects/#{@project.identifier}/wiki/Article"

      page = @project.wiki.pages.find_by_title("Article")
      assert_equal(<<-TEXT, page.content.text)
#{@controller.initial_page_content(page)}

#{@template_page.content.text}
      TEXT
    end

    test "template: content" do
      put :update, params: {
        project_id: @project.id,
        id: "Article",
        content: {
          comments: "create",
          text: "h1. Article\n\nCreated!\n",
        },
        template: @template_page.title,
        wiki_page: {
          parent_id: @wiki_page.id,
        },
      }
      assert_redirected_to "/projects/#{@project.identifier}/wiki/Article"

      page = @project.wiki.pages.find_by_title("Article")
      assert_equal("h1. Article\n\nCreated!\n", page.content.text)
    end
  end
end

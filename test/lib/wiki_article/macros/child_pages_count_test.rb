# Copyright (C) 2024  Kodama Takuya <otegami@clear-code.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

require File.dirname(__FILE__) + '/../../../test_helper'

module WikiArticle
  module Macros
    class ChildPagesCountTest < Redmine::HelperTest
      include ERB::Util

      fixtures :projects, :wikis, :wiki_pages, :wiki_contents, :wiki_content_versions, :issues,
               :roles, :enabled_modules, :users

      def setup
        super
        @project = Project.find(1)
      end

      def test_in_wiki_content
        wiki_page = WikiPage.find(2)
        assert_equal '<p>3</p>', textilizable('{{child_pages_count}}', object: wiki_page.content)
      end

      def test_in_wiki_content_version
        wiki_page = WikiPage.find(2)
        assert_equal '<p>3</p>', textilizable('{{child_pages_count}}', object: wiki_page.content.versions.first)
      end

      def test_with_child_page
        assert_equal '<p>1</p>', textilizable('{{child_pages_count(Child_1)}}')
      end

      def test_with_depth_option
        wiki_page = WikiPage.find(2)
        assert_equal '<p>2</p>', textilizable('{{child_pages_count(depth=1)}}', object: wiki_page.content)
      end

      def test_in_issue_page
        macro_error_message = <<-MESSAGE.gsub("\n", "")
<p>
<div class="flash error">
Error executing the <strong>child_pages_count</strong> macro
 (This {{child_pages_count}} macro can be called from Wiki pages only)
</div>
</p>
        MESSAGE
        assert_equal macro_error_message, textilizable('{{child_pages_count}}', object: Issue.first)
      end

      def test_in_another_porject_wiki_without_permissions
        another_project_wiki = Project.find(2).wiki.pages.first
        macro_error_message = <<~MESSAGE.gsub("\n", "")
<p>
<div class="flash error">
Error executing the <strong>child_pages_count</strong> macro (Page not found)
</div>
</p>
        MESSAGE
        assert_equal macro_error_message, textilizable('{{child_pages_count}}', object: another_project_wiki.content)
      end
    end
  end
end

# Copyright (C) 2024  Sutou Kouhei <kou@clear-code.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

require File.dirname(__FILE__) + '/../../../test_helper'

module WikiArticle
  module Macros
    class NewArticleTest < Redmine::HelperTest
      include ERB::Util

      fixtures :projects, :wikis, :wiki_pages, :wiki_contents, :wiki_content_versions, :issues,
               :roles, :enabled_modules, :users

      def setup
        super
        User.current = User.find(2)
        @project = Project.find(1)
        @wiki_page = WikiPage.find(2)
        @wiki_content = @wiki_page.content
      end

      test "default" do
        html = textilizable('{{new_article}}', object: @wiki_content)
        href = new_project_wiki_article_path(project_id: @project.identifier,
                                             parent: @wiki_page.title)
        assert_equal(<<-HTML.gsub("\n", ""), html)
<p>
<a class="icon icon-add" data-remote="true" href="#{h(href)}">
New article
</a>
</p>
        HTML
      end

      test "title" do
        html = textilizable('{{new_article(title=NewArticle)}}',
                            object: @wiki_content)
        href = new_project_wiki_article_path(project_id: @project.identifier,
                                             parent: @wiki_page.title,
                                             title: "NewArticle")
        assert_equal(<<-HTML.gsub("\n", ""), html)
<p>
<a class="icon icon-add" data-remote="true" href="#{h(href)}">
New article
</a>
</p>
        HTML
      end

      test "template" do
        html = textilizable('{{new_article(template=ArticleTemplate)}}',
                            object: @wiki_content)
        href = new_project_wiki_article_path(project_id: @project.identifier,
                                             parent: @wiki_page.title,
                                             template: "ArticleTemplate")
        assert_equal(<<-HTML.gsub("\n", ""), html)
<p>
<div class="flash error">
Error executing the <strong>new_article</strong> macro (Template ArticleTemplate not found)
</div>
</p>
        HTML
      end

      test "modal" do
        html = textilizable('{{new_article(modal=false)}}',
                            object: @wiki_content)
        href = new_project_wiki_article_path(project_id: @project.identifier,
                                             parent: @wiki_page.title)
        assert_equal(<<-HTML.gsub("\n", ""), html)
<p>
<a class="icon icon-add" href="#{h(href)}">
New article
</a>
</p>
        HTML
      end
    end
  end
end

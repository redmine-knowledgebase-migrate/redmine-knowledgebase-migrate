# Copyright (C) 2024  Abe Tomoaki <abe@clear-code.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

require 'tmpdir'
require File.expand_path('../../../test_helper', __FILE__)

module WikiArticle
  class KnowledgebaseImporterTest < Redmine::HelperTest
    fixtures :enabled_modules, :issues, :news, :projects, :users,
      :wikis, :wiki_contents, :wiki_content_versions, :wiki_pages

    plugin_fixtures :attachments, :comments, :watchers,
      :kb_articles, :kb_article_versions, :kb_categories, :tags, :taggings, :viewings,
      :wiki_extensions_comments, :wiki_extensions_counts,
      :wiki_extensions_settings, :wiki_extensions_tags, :wiki_extensions_tag_relations

    def setup
      Project.where(id: [1, 2, 3, 4, 6]).each do |project|
        project.enable_module!(:knowledgebase)
        project.enable_module!(:wiki)
        project.enable_module!(:wiki_extensions)
      end
      # For project 3, 4 and 6, create the start page since it does not exist.
      Wiki.create(project: Project.find(3), start_page: 'Wiki')
      Project.where(id: [3, 4, 6]).each do |project|
        page = WikiPage.new(wiki: project.wiki, title: project.wiki.start_page)
        content = WikiContent.new(page: page)
        content.text = 'text'
        content.author_id = 1
        page.save_with_content(content)
      end

      @env_keys = [
        'ARTICLE_TEMPLATE_NAME',
        'CONTENT_DIR',
        'REDMINE_LANG',
        'TEXT_FORMATTING',
        'TOP_PAGE_TITLE'
      ]
      @env_keep = {}
      @env_keys.each do |name|
        @env_keep[name], ENV[name] = ENV[name], nil
      end

      @importer = KnowledgebaseImporter.new
    end

    def teardown
      @env_keys.each do |name|
        ENV[name] = @env_keep[name]
      end
    end

    class GenericTest < self
      test 'target_projects' do
        assert_equal(
          Project.where(id: [1, 2, 3, 4, 6]).to_a,
          @importer.send(:target_projects)
        )
      end

      test 'alert_projects' do
        Project.find(1).disable_module!(:wiki)
        Project.find(2).disable_module!(:wiki_extensions)
        Project.find(3).disable_module!(:wiki)
        Project.find(3).disable_module!(:wiki_extensions)

        assert_equal(
          Project.where(id: [1, 2, 3]).to_a,
          @importer.send(:alert_projects)
        )
      end

      test 'content_dir: default' do
        assert_equal(
          File.expand_path(File.join(__dir__, '..', '..', '..', 'examples', 'en')),
          @importer.send(:content_dir)
        )
      end

      test 'content_dir: REDMINE_LANG specified' do
        ENV['REDMINE_LANG'] = 'ja'
        assert_equal(
          File.expand_path(File.join(__dir__, '..', '..', '..', 'examples', 'ja')),
          @importer.send(:content_dir)
        )
      end

      test 'content_dir: CONTENT_DIR specified' do
        ENV['CONTENT_DIR'] = '/tmp'
        assert_equal(
          '/tmp',
          @importer.send(:content_dir)
        )
      end

      test 'suffix: default' do
        assert_equal('textile', @importer.send(:suffix))
      end

      test 'suffix: specify' do
        ENV['TEXT_FORMATTING'] = 'md'
        assert_equal('md', @importer.send(:suffix))
      end

      test 'knowledgebase_top_content_file: default' do
        assert_equal(
          File.expand_path(File.join(__dir__, '..', '..', '..', 'examples', 'en', 'knowledgebase-top.textile')),
          @importer.send(:knowledgebase_top_content_file)
        )
      end

      test 'knowledgebase_top_content_file: specify' do
        ENV['CONTENT_DIR'] = '/tmp'
        ENV['TEXT_FORMATTING'] = 'md'
        assert_equal(
          File.expand_path(File.join('/tmp', 'knowledgebase-top.md')),
          @importer.send(:knowledgebase_top_content_file)
        )
      end

      test 'category_top_template_file: default' do
        assert_equal(
          File.expand_path(File.join(__dir__, '..', '..', '..', 'examples', 'en', 'category-top.textile')),
          @importer.send(:category_top_template_file)
        )
      end

      test 'category_top_template_file: specify' do
        ENV['CONTENT_DIR'] = '/tmp'
        ENV['TEXT_FORMATTING'] = 'md'
        assert_equal(
          File.expand_path(File.join('/tmp', 'category-top.md')),
          @importer.send(:category_top_template_file)
        )
      end

      test 'article_template_file: default' do
        assert_equal(
          File.expand_path(File.join(__dir__, '..', '..', '..', 'examples', 'en', 'article-template.textile')),
          @importer.send(:article_template_file)
        )
      end

      test 'article_template_file: specify' do
        ENV['CONTENT_DIR'] = '/tmp'
        ENV['TEXT_FORMATTING'] = 'md'
        assert_equal(
          File.expand_path(File.join('/tmp', 'article-template.md')),
          @importer.send(:article_template_file)
        )
      end

      test 'category_page_name' do
        assert_equal(
          'Category-category-title',
          @importer.send(:category_page_name, KbCategory.new(title: 'category-title'))
        )
      end

      test 'article_text: default TEXT_FORMATTING' do
        template = "h2. %{summary}\nh2. %{description}"
        article = KbArticle.new(
          title: 'TEST-TITLE',
          summary: 'TEST-SUMMARY',
          content: 'TEST-DESCRIPTION'
        )
        assert_equal(
          "h1. TEST-TITLE\nh2. TEST-SUMMARY\nh2. TEST-DESCRIPTION",
          @importer.send(:article_text, article, template)
        )
      end

      test 'article_text: TEXT_FORMATTING=md' do
        ENV['TEXT_FORMATTING'] = 'md'
        template = "## %{summary}\n## %{description}"
        article = KbArticle.new(
          title: 'TEST-TITLE',
          summary: 'TEST-SUMMARY',
          content: 'TEST-DESCRIPTION'
        )
        assert_equal(
          "# TEST-TITLE\n## TEST-SUMMARY\n## TEST-DESCRIPTION",
          @importer.send(:article_text, article, template)
        )
      end
    end

    class ValidateEnabledModulesTest < self
      test 'valid' do
        @importer.validate_enabled_modules
      end

      test 'invalid (Wiki is disabled)' do
        Project.find(1).disable_module!(:wiki)

        e = assert_raises KnowledgebaseImporter::ValidationError do
          @importer.validate_enabled_modules
        end
        assert_equal('The target projects must enable "wiki" and "wiki_extensions": eCookbook (1)', e.message)
      end
    end

    class ValidateWikiStartPageTest < self
      test 'valid' do
        @importer.validate_wiki_start_page
      end

      test 'invalid' do
        project = Project.find(4)
        project.wiki.find_page(project.wiki.start_page).delete

        e = assert_raises KnowledgebaseImporter::ValidationError do
          @importer.validate_wiki_start_page
        end
        assert_equal(
          'Some projects have not yet created a Wiki start page: eCookbook Subproject 2 (4)',
          e.message
        )
      end
    end

    class ValidateContentFilesTest < self
      test 'valid' do
        @importer.validate_content_files
      end

      test 'invalid' do
        Dir.mktmpdir do |dir|
          ENV['CONTENT_DIR'] = dir

          e = assert_raises KnowledgebaseImporter::ValidationError do
            @importer.validate_content_files
          end
          error_files = [
            'knowledgebase-top.textile',
            'category-top.textile',
            'article-template.textile'
          ].collect {|file_name| File.join(dir, file_name) }
          assert_equal(
            "Could not find template files: #{error_files.join(', ')}",
            e.message
          )
        end
      end
    end

    class ValidateDuplicatedArticleTitle < self
      test 'valid' do
        @importer.validate_duplicated_article_title
      end

      test 'invalid' do
        duplicated_article = KbArticle.find(1).dup
        duplicated_article.save!

        e = assert_raises KnowledgebaseImporter::ValidationError do
          @importer.validate_duplicated_article_title
        end
        error_info =
          "project=#{duplicated_article.project.name}(#{duplicated_article.project.id}) title=#{duplicated_article.title}"
        assert_equal(
          "There are articles with the same title in the same project: #{error_info}",
          e.message
        )
      end
    end

    class CreateWikiPageTest < self
      def setup
        super

        title = 'Already exists'
        @importer.send(
          :create_wiki_page,
          User.find(1),
          Project.find(1).wiki,
          title,
          title + 'text'
        )
      end

      test 'without parent' do
        @importer.send(
          :create_wiki_page,
          User.find(1),
          Project.find(1).wiki,
          'title',
          'text'
        )

        page = Project.find(1).wiki.find_page('title')
        assert_equal(
          {
            parent: nil,
            author_id: 1,
            text: 'text'
          },
          {
            parent: page.parent,
            author_id: page.content.author_id,
            text: page.content.text
          }
        )
      end

      test 'with parent' do
        @importer.send(
          :create_wiki_page,
          User.find(1),
          Project.find(1).wiki,
          'title',
          'text',
          parent: Project.find(1).wiki.find_page('CookBook_documentation')
        )

        page = Project.find(1).wiki.find_page('title')
        assert_equal(
          {
            parent: Project.find(1).wiki.find_page('CookBook_documentation'),
            author_id: 1,
            text: 'text'
          },
          {
            parent: page.parent,
            author_id: page.content.author_id,
            text: page.content.text
          }
        )
      end

      test 'Exception: Already exists' do
        e = assert_raises ActiveRecord::RecordInvalid do
          title = 'Already exists'
          @importer.send(
            :create_wiki_page,
            User.find(1),
            Project.find(1).wiki,
            title,
            'add'
          )
        end
        assert_equal('Validation failed: Title has already been taken', e.message)
      end
    end

    class CreateTemplatePageTest < self
      def setup
        super

        title = 'Already exists'
        @importer.send(
          :create_wiki_page,
          User.find(2),
          Project.find(1).wiki,
          title,
          title + ' text'
        )
      end

      test 'new page' do
        @importer.send(
          :create_template_page,
          User.find(1),
          Project.find(1).wiki,
          'title',
          "h2. %{summary}\nh2. %{description}"
        )

        page = Project.find(1).wiki.find_page('title')
        assert_equal(
          {
            parent: nil,
            author_id: 1,
            text: "h2. %{summary}\nh2. %{description}"
          },
          {
            parent: page.parent,
            author_id: page.content.author_id,
            text: page.content.text
          }
        )
      end

      test 'already exists' do
        title = 'Already exists'
        @importer.send(
          :create_template_page,
          User.find(1),
          Project.find(1).wiki,
          title,
          "h2. %{summary}\nh2. %{description} RERUN"
        )
        page = Project.find(1).wiki.find_page(title)
        assert_equal(
          {
            parent: nil,
            author_id: 2,
            text: "Already exists text"
          },
          {
            parent: page.parent,
            author_id: page.content.author_id,
            text: page.content.text
          }
        )
      end
    end

    class EnsureKnowledgebaseTopTest < self
      def setup
        super

        @project = Project.find(1)
        @start_page = @project.wiki.find_page(@project.wiki.start_page)

        title = 'Knowledgebase Top already there'
        @importer.send(
          :ensure_knowledgebase_top,
          User.find(1),
          Project.find(1),
          'Knowledgebase Top TEXT already there',
          title: title,
        )
      end

      test 'default title' do
        @importer.send(
          :ensure_knowledgebase_top,
          User.find(1),
          @project,
          'Knowledgebase Top TEXT'
        )

        page = @project.wiki.find_page(KnowledgebaseImporter::DEFAULT_KNOWLEDGEBASE_TOP_TITLE)
        assert_equal(
          {
            parent: @start_page,
            text: 'Knowledgebase Top TEXT'
          },
          {
            parent: page.parent,
            text: page.content.text
          }
        )
      end

      test 'specify title' do
        title = 'ensure_knowledgebase_top: specify title'
        @importer.send(
          :ensure_knowledgebase_top,
          User.find(1),
          @project,
          'Knowledgebase Top TEXT',
          title: title,
        )

        page = @project.wiki.find_page(title)
        assert_equal(
          {
            parent: @start_page,
            text: 'Knowledgebase Top TEXT'
          },
          {
            parent: page.parent,
            text: page.content.text
          }
        )
      end

      test 'already exists' do
        title = 'Knowledgebase Top already there'
        @importer.send(
          :ensure_knowledgebase_top,
          User.find(1),
          @project,
          'Knowledgebase Top TEXT new',
          title: title
        )

        page = @project.wiki.find_page(title)
        assert_equal(
          {
            parent: @start_page,
            text: 'Knowledgebase Top TEXT already there',
          },
          {
            parent: page.parent,
            text: page.content.text
          }
        )
      end
    end

    class CreateCategoryPageTest < self
      def setup
        super

        @project = Project.find(2)
        @author = User.find(2)
        @kb_top = @importer.send(:ensure_knowledgebase_top, @author, @project, 'TEST')

        title = 'Category-Already exists'
        @importer.send(
          :create_wiki_page,
          @author,
          @project.wiki,
          title,
          title + 'text'
        )
      end

      test 'success' do
        category = KbCategory.find(2)
        @importer.send(
          :create_category_page,
          @author,
          @project.wiki,
          "Category: %{name}\n...category...",
          category,
          @kb_top
        )

        title = @importer.send(:category_page_name, category)
        page = @project.wiki.find_page(title)
        assert_equal(
          {
            parent: @kb_top,
            text: "Category: category2\n...category...",
          },
          {
            parent: page.parent,
            text: page.content.text
          }
        )
      end

      test 'Exception: Already exists' do
        e = assert_raises ActiveRecord::RecordInvalid do
          @importer.send(
            :create_category_page,
            @author,
            @project.wiki,
            "Category: %{name}\n...category...",
            KbCategory.new(title: 'Already exists'),
            @kb_top
          )
        end
        assert_equal('Validation failed: Title has already been taken', e.message)
      end
    end

    class ImportCategoriesTest < self
      def setup
        super

        @project = Project.find(2)
        @author = User.find(2)
        @importer.send(:ensure_knowledgebase_top, @author, @project, 'TEST')
        @kb_top = @project.wiki.find_page(KnowledgebaseImporter::DEFAULT_KNOWLEDGEBASE_TOP_TITLE)

        title = 'Category-Already exists'
        @importer.send(
          :create_wiki_page,
          @author,
          @project.wiki,
          title,
          title + ' text'
        )
      end

      test 'Create all pages' do
        @importer.send(
          :import_categories,
          @author,
          @project.wiki,
          "Category: %{name}\n...category...",
          @project.categories,
          @kb_top
        )

        page_category2 = @project.wiki.find_page('Category-category2')
        assert_equal(
          {
            parent: @kb_top,
            text: "Category: category2\n...category..."
          },
          {
            parent: page_category2.parent,
            text: page_category2.content.text
          }
        )

        page_category3 = @project.wiki.find_page('Category-category3')
        assert_equal(
          {
            parent: page_category2,
            text: "Category: category3\n...category..."
          },
          {
            parent: page_category3.parent,
            text: page_category3.content.text
          }
        )
      end

      test 'Exists on some pages' do
        category = KbCategory.create(title: 'Already exists')
        category.project = @project
        category.save!

        @importer.send(
          :import_categories,
          @author,
          @project.wiki,
          "Category: %{name}\n...category...",
          @project.categories,
          @kb_top
        )

        page_already_exists = @project.wiki.find_page('Category-Already exists')
        assert_equal(
          {
            parent: nil,
            text: "Category-Already exists text"
          },
          {
            parent: page_already_exists.parent,
            text: page_already_exists.content.text
          }
        )

        page_does_not_exist = @project.wiki.find_page('Category-category2')
        assert_equal(
          {
            parent: @kb_top,
            text: "Category: category2\n...category..."
          },
          {
            parent: page_does_not_exist.parent,
            text: page_does_not_exist.content.text
          }
        )
      end

      test 'import_category_watchers' do
        @importer.send(
          :import_categories,
          @author,
          @project.wiki,
          "Category: %{name}\n...category...",
          @project.categories,
          @kb_top
        )

        @importer.send(:import_category_watchers, @project)
        KbCategory.where(id: [2, 3]).each do |category|
          page = @project.wiki.find_page(@importer.send(:category_page_name, category))
          assert_equal(
            category.watchers.collect(&:user_id),
            page.watchers.collect(&:user_id)
          )
        end
      end
    end

    class ImportArticlesTest < self
      def setup
        super

        @project = Project.find(2)
        @importer.send(
          :import_categories,
          User.find(2),
          @project.wiki,
          "h1. %{name}\nTEXT",
          @project.categories,
          @project.wiki.find_page(@project.wiki.start_page)
        )
        @target_page = @importer.send(
          :create_wiki_page,
          User.find(2),
          @project.wiki,
          'title',
          'text'
        )
        @template = "h2. %{summary}\nh2. %{description}"
      end

      test 'import_article_versions' do
        article = KbArticle.find(6)

        # Deleted for automatic versioning.
        WikiContentVersion.where(wiki_content_id: @target_page.content.id).delete_all

        @importer.send(
          :import_article_versions,
          article,
          @target_page,
          @template
        )

        actual_versions = WikiContentVersion.where(wiki_content_id: @target_page.content.id)
        assert_equal(3, actual_versions.count)
        actual_versions.each do |actual|
          expected = article.versions.find_by_version(actual.version)
          assert_equal(
            {
              text: @importer.send(:article_text, expected, @template),
              page_id: @target_page.id,
              author_id: expected.version == 1 ? expected.author_id : expected.updater_id,
              comments: expected.version_comments,
              updated_on: expected.updated_at
            },
            {
              text: actual.data,
              page_id: actual.page_id,
              author_id: actual.author_id,
              comments: actual.comments,
              updated_on: actual.updated_on
            }
          )
        end
      end

      test 'import_article_versions: Same version exists' do
        article = KbArticle.find(6)

        @importer.send(
          :import_article_versions,
          article,
          @target_page,
          @template
        )

        actual_versions = WikiContentVersion.where(wiki_content_id: @target_page.content.id)
        assert_equal(3, actual_versions.count)

        # version=1 already exists since `create_wiki_page` in setup.
        assert_equal(
          @target_page.content.versions.find_by_version(1),
          actual_versions.find_by_version(1)
        )
        actual_versions.where.not(version: 1).each do |actual|
          expected = article.versions.find_by_version(actual.version)
          assert_equal(
            {
              text: @importer.send(:article_text, expected, @template),
              page_id: @target_page.id,
              author_id: expected.updater_id,
              comments: expected.version_comments,
              updated_on: expected.updated_at
            },
            {
              text: actual.data,
              page_id: actual.page_id,
              author_id: actual.author_id,
              comments: actual.comments,
              updated_on: actual.updated_on
            }
          )
        end
      end

      test 'import_tags' do
        article = KbArticle.find(2)
        @importer.send(
          :import_tags,
          article,
          @target_page,
        )
        assert_equal(
          article.tags.collect(&:name),
          @target_page.wiki_ext_tags.collect(&:name)
        )
      end

      test 'import_attachments' do
        article = KbArticle.find(4)
        @importer.send(
          :import_attachments,
          article,
          @target_page,
        )
        assert_equal(
          article.attachments.collect {|a| a.attributes.except('id', 'container_id', 'container_type') },
          @target_page.attachments.collect {|a| a.attributes.except('id', 'container_id', 'container_type') }
        )
      end

      test 'import_attachments: includes locked user' do
        article = KbArticle.find(4)
        target_user = article.attachments.first.author
        target_user.lock!
        @importer.send(
          :import_attachments,
          article,
          @target_page,
        )
        assert_equal(
          article.attachments.collect {|a| a.attributes.except('id', 'container_id', 'container_type') },
          @target_page.attachments.collect {|a| a.attributes.except('id', 'container_id', 'container_type') }
        )
      end

      test 'import_attachments: includes destroyed user' do
        article = KbArticle.find(4)
        target_user = article.attachments.first.author
        target_user.destroy!
        @importer.send(
          :import_attachments,
          article,
          @target_page,
        )
        assert_equal(
          article.attachments.collect {|a| a.attributes.except('id', 'container_id', 'container_type') },
          @target_page.attachments.collect {|a| a.attributes.except('id', 'container_id', 'container_type') }
        )
      end

      test 'import_attachments: Same data exists' do
        article = KbArticle.find(4)
        expected = article.attachments.collect {|a| a.attributes.except('id', 'container_id', 'container_type') }.to_a

        # Same result after multiple runs.
        2.times do |i|
          @importer.send(
            :import_attachments,
            article,
            @target_page,
          )
          assert_equal(
            expected,
            @target_page.attachments.collect {|a| a.attributes.except('id', 'container_id', 'container_type') },
            "#{i} times"
          )
        end
      end

      test 'import_comments' do
        article = KbArticle.find(1)
        @importer.send(
          :import_comments,
          article,
          @target_page,
        )

        actual = WikiExtensionsComment.where(wiki_page_id: @target_page.id).order(:updated_at)
        assert_equal(
          article.comments.order(:updated_on).collect do |comment|
            {
              user_id: comment.author_id,
              comment: comment.content,
              created_at: comment.created_on,
              updated_at: comment.updated_on,
              parent_id: nil,
            }
          end,
          actual.collect do |comment|
            {
              user_id: comment.user_id,
              comment: comment.comment,
              created_at: comment.created_at,
              updated_at: comment.updated_at,
              parent_id: comment.parent_id
            }
          end
        )
      end

      test 'import_comments: includes locked user' do
        article = KbArticle.find(1)
        target_user = article.comments.first.author
        target_user.lock!

        @importer.send(
          :import_comments,
          article,
          @target_page,
        )

        actual = WikiExtensionsComment.where(wiki_page_id: @target_page.id).order(:updated_at)
        assert_equal(
          article.comments.order(:updated_on).collect do |comment|
            {
              user_id: comment.author_id,
              comment: comment.content,
              created_at: comment.created_on,
              updated_at: comment.updated_on,
              parent_id: nil,
            }
          end,
          actual.collect do |comment|
            {
              user_id: comment.user_id,
              comment: comment.comment,
              created_at: comment.created_at,
              updated_at: comment.updated_at,
              parent_id: comment.parent_id
            }
          end
        )
      end

      test 'import_comments: includes destroyed user' do
        article = KbArticle.find(1)
        target_user = article.comments.first.author
        target_user.destroy!

        @importer.send(
          :import_comments,
          article,
          @target_page,
        )

        actual = WikiExtensionsComment.where(wiki_page_id: @target_page.id).order(:updated_at)
        assert_equal(
          article.comments.order(:updated_on).collect do |comment|
            {
              user_id: comment.author_id,
              comment: comment.content,
              created_at: comment.created_on,
              updated_at: comment.updated_on,
              parent_id: nil,
            }
          end,
          actual.collect do |comment|
            {
              user_id: comment.user_id,
              comment: comment.comment,
              created_at: comment.created_at,
              updated_at: comment.updated_at,
              parent_id: comment.parent_id
            }
          end
        )
      end

      test 'import_comments: same data except' do
        article = KbArticle.find(1)
        expected = article.comments.order(:updated_on).collect do |comment|
          {
            user_id: comment.author_id,
            comment: comment.content,
            created_at: comment.created_on,
            updated_at: comment.updated_on,
            parent_id: nil,
          }
        end

        # Same result after multiple runs.
        2.times do |i|
          @importer.send(
            :import_comments,
            article,
            @target_page,
          )

          actual = WikiExtensionsComment.where(wiki_page_id: @target_page.id).order(:updated_at)
          assert_equal(
            expected,
            actual.collect do |comment|
              {
                user_id: comment.user_id,
                comment: comment.comment,
                created_at: comment.created_at,
                updated_at: comment.updated_at,
                parent_id: comment.parent_id
              }
            end,
            "#{i} times"
          )
        end
      end

      test 'import_view_count' do
        article = KbArticle.find(1)
        @importer.send(
          :import_view_count,
          article,
          @target_page,
        )
        assert_equal(
          [
            {
              project_id: 2,
              date: '2024-03-01',
              count: 2
            },
            {
              project_id: 2,
              date: '2024-03-02',
              count: 2
            },
            {
              project_id: 2,
              date: '2024-03-03',
              count: 1
            }
          ],
          WikiExtensionsCount.where(page_id: @target_page.id).order(:date).collect do |count|
            {
              project_id: count.project_id,
              date: count.date.to_s,
              count: count.count
            }
          end
        )
      end

      test 'import_view_count: Same data exists' do
        article = KbArticle.find(1)
        expected = [
          {
            project_id: 2,
            date: '2024-03-01',
            count: 2
          },
          {
            project_id: 2,
            date: '2024-03-02',
            count: 2
          },
          {
            project_id: 2,
            date: '2024-03-03',
            count: 1
          }
        ]

        # Same result after multiple runs.
        2.times do |i|
          @importer.send(
            :import_view_count,
            article,
            @target_page,
          )
          assert_equal(
            expected,
            WikiExtensionsCount.where(page_id: @target_page.id).order(:date).collect do |count|
              {
                project_id: count.project_id,
                date: count.date.to_s,
                count: count.count
              }
            end,
            "#{i} times"
          )
        end
      end

      test 'import_watchers' do
        article = KbArticle.find(2)
        @importer.send(
          :import_watchers,
          article,
          @target_page,
        )
        assert_equal(
          article.watchers.collect(&:user_id),
          @target_page.watchers.collect(&:user_id)
        )
      end

      test 'import_watchers: includes locked user' do
        article = KbArticle.find(2)
        article.watchers.find_by(user_id: 2).user.lock!
        @importer.send(
          :import_watchers,
          article,
          @target_page,
        )
        assert_equal(
          [10],
          @target_page.watchers.collect(&:user_id)
        )
      end

      # There is no lock on Group.
      # test 'import_watchers: includes locked group' do
      # end
      #
      test 'import_watchers: includes destroyed user' do
        article = KbArticle.find(2)
        article.watchers.find_by(user_id: 2).user.destroy!
        @importer.send(
          :import_watchers,
          article,
          @target_page,
        )
        assert_equal(
          [10],
          @target_page.watchers.collect(&:user_id)
        )
      end

      test 'import_watchers: includes destroyed group' do
        article = KbArticle.find(2)
        article.watchers.find_by(user_id: 10).user.destroy!
        @importer.send(
          :import_watchers,
          article,
          @target_page,
        )
        assert_equal(
          [2],
          @target_page.watchers.collect(&:user_id)
        )
      end

      test 'import_watchers: Same data exists' do
        article = KbArticle.find(2)
        expected = article.watchers.collect(&:user_id)

        # Same result after multiple runs.
        2.times do |i|
          @importer.send(
            :import_watchers,
            article,
            @target_page,
          )
          assert_equal(
            expected,
            @target_page.watchers.collect(&:user_id),
            "#{i} times"
          )
        end
      end

      test 'import_article (for version=1 only)' do
        article = KbArticle.find(5)
        @importer.send(
          :import_article,
          @project.wiki,
          article,
          @template
        )

        # WikiPage
        actual_page = @project.wiki.find_page(article.title)
        assert_equal(
          {
            wiki_id: @project.wiki.id,
            created_on: article.created_at,
            parent: @project.wiki.find_page(@importer.send(:category_page_name, article.category))
          },
          {
            wiki_id: actual_page.wiki_id,
            created_on: actual_page.created_on,
            parent: actual_page.parent
          }
        )

        # WikiContent
        actual_content = actual_page.content
        assert_equal(
          {
            author_id: article.author_id,
            text: "h1. title5\nh2. summary5\n...\nh2. content5\n...",
            comments: article.version_comments,
            updated_on: article.updated_at,
            version: article.version
          },
          {
            author_id: actual_content.author_id,
            text: actual_content.text,
            comments: actual_content.comments,
            updated_on: actual_content.updated_on,
            version: actual_content.version
          }
        )

        # WikiContentVersion
        assert_equal(
          [article.version], # Only the latest version
          actual_content.versions.collect(&:version).sort
        )
      end

      def assert_article_id_6(article)
        @project.articles.each do |article|
          assert @project.wiki.find_page(article.title)
        end

        # Test of details about article_id=6
        # WikiPage
        actual_page = @project.wiki.find_page(article.title)
        assert_equal(
          {
            wiki_id: @project.wiki.id,
            created_on: article.created_at,
            parent: @project.wiki.find_page(@importer.send(:category_page_name, article.category))
          },
          {
            wiki_id: actual_page.wiki_id,
            created_on: actual_page.created_on,
            parent: actual_page.parent
          }
        )

        # WikiContent
        actual_content = actual_page.content
        assert_equal(
          {
            author_id: article.updater_id,
            text: "h1. title6\nh2. summary6\n...\nh2. content6\n...",
            comments: article.version_comments,
            updated_on: article.updated_at,
            version: article.version
          },
          {
            author_id: actual_content.author_id,
            text: actual_content.text,
            comments: actual_content.comments,
            updated_on: actual_content.updated_on,
            version: actual_content.version
          }
        )

        # WikiContentVersion
        assert_equal(
          article.versions.collect(&:version).sort,
          actual_content.versions.collect(&:version).sort
        )

        # Tag, Tagging
        assert_equal(
          article.tags.collect(&:name),
          actual_page.wiki_ext_tags.collect(&:name)
        )

        # Attachment
        assert_equal(
          article.attachments.collect {|a| a.attributes.except('id', 'container_id', 'container_type') },
          actual_page.attachments.collect {|a| a.attributes.except('id', 'container_id', 'container_type') }
        )


        # WikiExtensionsComment
        assert_equal(
          article.comments.order(:updated_on).collect do |comment|
            {
              user_id: comment.author_id,
              comment: comment.content,
              created_at: comment.created_on,
              updated_at: comment.updated_on,
              parent_id: nil,
            }
          end,
          WikiExtensionsComment.where(wiki_page_id: actual_page.id).order(:updated_at).collect do |comment|
            {
              user_id: comment.user_id,
              comment: comment.comment,
              created_at: comment.created_at,
              updated_at: comment.updated_at,
              parent_id: comment.parent_id
            }
          end
        )

        # wiki_extensions_counts
        assert_equal(1, WikiExtensionsCount.access_count(actual_page.id))

        # Watcher
        assert_equal(
          article.watchers.collect(&:user_id),
          actual_page.watchers.collect(&:user_id)
        )
      end

      test 'import_articles' do
        article = KbArticle.find(6)
        @importer.send(
          :import_articles,
          @project,
          @template
        )

        assert_article_id_6(article)
      end

      test 're-run import_articles' do
        article = KbArticle.find(6)
        @importer.send(
          :import_articles,
          @project,
          @template
        )

        # RERUN
        2.times do |i|
          @importer.send(
            :import_articles,
            @project,
            "RERUN!!!\nh1. %{title}\nh2. %{summary}\nh2. %{description}"
          )

          assert_article_id_6(article)
        end
      end
    end

    class ImportArticlesTestWithInactiveuser < self
      # author testing only. Since that is the focus.

      def setup
        super

        @page = WikiPage.find(1)
        WikiContentVersion.where(wiki_content_id: @page.content.id).delete_all
      end

      test 'import_article: author is locked (for version=1 only)' do
        article = KbArticle.find(5)
        article.author.lock!
        article.reload

        wiki = article.project.wiki
        @importer.send(
          :import_article,
          wiki,
          article,
          ''
        )

        actual_content = wiki.find_page(article.title).content
        assert_equal(article.author, actual_content.author)
      end

      test 'import_article: author is destroyed (for version=1 only)' do
        article = KbArticle.find(5)
        article.author.destroy!
        article.reload

        wiki = article.project.wiki
        @importer.send(
          :import_article,
          wiki,
          article,
          ''
        )

        actual_content = wiki.find_page(article.title).content
        assert_equal(User.anonymous, actual_content.author)
      end

      test 'import_article: updater is locked (for version >= 2)' do
        article = KbArticle.find(7)
        article.updater.lock!
        article.reload

        wiki = article.project.wiki
        @importer.send(
          :import_article,
          wiki,
          article,
          ''
        )

        actual_content = wiki.find_page(article.title).content
        assert_equal(article.updater, actual_content.author)
      end

      test 'import_article: author is locked (for version >= 2)' do
        article = KbArticle.find(7)
        article.author.lock!
        article.reload

        wiki = article.project.wiki
        @importer.send(
          :import_article,
          wiki,
          article,
          ''
        )

        actual_content = wiki.find_page(article.title).content
        assert_equal(article.updater, actual_content.author)
      end

      test 'import_article: updater is destroyed (for version >= 2)' do
        article = KbArticle.find(7)
        article.updater.destroy!
        article.reload

        wiki = article.project.wiki
        @importer.send(
          :import_article,
          wiki,
          article,
          ''
        )

        actual_content = wiki.find_page(article.title).content
        assert_equal(User.anonymous, actual_content.author)
      end

      test 'import_article: author is destroyed (for version >= 2)' do
        # When author and updater are different.
        article = KbArticle.find(7)
        article.author.destroy!
        article.reload

        wiki = article.project.wiki
        @importer.send(
          :import_article,
          wiki,
          article,
          ''
        )

        actual_content = wiki.find_page(article.title).content
        assert_equal(article.updater, actual_content.author)
      end

      test 'import_article_versions: author is locked (for version=1 only)' do
        article = KbArticle.find(5)
        article.author.lock!
        article.reload

        @importer.send(
          :import_article_versions,
          article,
          @page,
          ''
        )

        actual_versions = WikiContentVersion.where(wiki_content_id: @page.content.id)
        assert_equal(
          [article.author],
          actual_versions.order(:version).collect(&:author)
        )
      end

      test 'import_article_versions: author is destroyed (for version=1 only)' do
        article = KbArticle.find(5)
        article.author.destroy!
        article.reload

        @importer.send(
          :import_article_versions,
          article,
          @page,
          ''
        )

        actual_versions = WikiContentVersion.where(wiki_content_id: @page.content.id)
        assert_equal(
          [User.anonymous],
          actual_versions.order(:version).collect(&:author)
        )
      end

      test 'import_article_versions: updater is locked (for version >= 2)' do
        article = KbArticle.find(7)
        # user_id=4 updated version=3.
        User.find(4).lock!
        article.reload

        @importer.send(
          :import_article_versions,
          article,
          @page,
          ''
        )

        actual_versions = WikiContentVersion.where(wiki_content_id: @page.content.id)
        assert_equal(
          [
            article.versions.find_by_version(1).author,
            article.versions.find_by_version(2).updater,
            article.versions.find_by_version(3).updater,
          ],
          actual_versions.order(:version).collect(&:author)
        )
      end

      test 'import_article_versions: author is locked (for version >= 2)' do
        article = KbArticle.find(7)
        # author creates an article (version=1). Also updated version=2.
        article.author.lock!
        article.reload

        @importer.send(
          :import_article_versions,
          article,
          @page,
          ''
        )

        actual_versions = WikiContentVersion.where(wiki_content_id: @page.content.id)
        assert_equal(
          [
            article.versions.find_by_version(1).author,
            article.versions.find_by_version(2).updater,
            article.versions.find_by_version(3).updater,
          ],
          actual_versions.order(:version).collect(&:author)
        )
      end

      test 'import_article_versions: updater is destroyed (for version >= 2)' do
        article = KbArticle.find(7)
        # user_id=4 updated version=3.
        User.find(4).destroy!
        article.reload

        @importer.send(
          :import_article_versions,
          article,
          @page,
          ''
        )

        actual_versions = WikiContentVersion.where(wiki_content_id: @page.content.id)
        assert_equal(
          [
            article.versions.find_by_version(1).author,
            article.versions.find_by_version(2).updater,
            User.anonymous
          ],
          actual_versions.order(:version).collect(&:author)
        )
      end

      test 'import_article_versions: author is destroyed (for version >= 2)' do
        article = KbArticle.find(7)
        # author creates an article (version=1). Also updated version=2.
        article.author.destroy!
        article.reload

        @importer.send(
          :import_article_versions,
          article,
          @page,
          ''
        )

        actual_versions = WikiContentVersion.where(wiki_content_id: @page.content.id)
        assert_equal(
          [
            User.anonymous,
            User.anonymous,
            article.versions.find_by_version(3).updater,
          ],
          actual_versions.order(:version).collect(&:author)
        )
      end
    end

    class ImportTest < self
      test 'default knowledgebase top title' do
        @importer.import

        Project.where(id: [1, 2, 3, 4, 6]).each do |project|
          assert project.wiki.find_page(KnowledgebaseImporter::DEFAULT_KNOWLEDGEBASE_TOP_TITLE)

          project.categories.each do |category|
            title = @importer.send(:category_page_name, category)
            page = project.wiki.find_page(title)

            assert_equal(
              category.watchers.collect(&:user_id),
              page.watchers.collect(&:user_id)
            )
          end

          project.articles.each do |article|
            assert project.wiki.find_page(article.title)
          end

          assert project.wiki.find_page(KnowledgebaseImporter::DEFAULT_ARTICLE_TEMPLATE_TITLE)
        end
      end

      test 'specify knowledgebase top title' do
        ENV['TOP_PAGE_TITLE'] = 'TITLE'
        @importer.import

        Project.where(id: [1, 2, 3, 4, 6]).each do |project|
          assert project.wiki.find_page('TITLE')

          project.categories.each do |category|
            title = @importer.send(:category_page_name, category)
            page = project.wiki.find_page(title)

            assert_equal(
              category.watchers.collect(&:user_id),
              page.watchers.collect(&:user_id)
            )
          end

          project.articles.each do |article|
            assert project.wiki.find_page(article.title)
          end

          assert project.wiki.find_page(KnowledgebaseImporter::DEFAULT_ARTICLE_TEMPLATE_TITLE)
        end
      end

      test 'specify article template title' do
        ENV['ARTICLE_TEMPLATE_NAME'] = 'Template-2024-03-22'
        @importer.import

        Project.where(id: [1, 2, 3, 4, 6]).each do |project|
          assert project.wiki.find_page(KnowledgebaseImporter::DEFAULT_KNOWLEDGEBASE_TOP_TITLE)
          assert project.wiki.find_page('Template-2024-03-22')
        end
      end
    end
  end
end

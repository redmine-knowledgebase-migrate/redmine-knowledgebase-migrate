# CHANGELOG

## 1.1.1 - 2024-05-13

Changed to run the job after the import is finished.

If an asynchronous job is run during import and it fails, it gets stuck.
Therefore, we changed the job to be run after the import is finished.

## 1.1.0 - 2024-04-25

* Improved error handling when `new_article` is set to a page.
* Fixed an error when importing data of an inactive user.
* Fixed to the correct last updated user.

## 1.0.0 - 2024-03-26

Initial release!!!
